Run `npm start` to start server on port 3001.

Endpoints:
- `/startMatch` : starts a new match.
- `/endMatch` : ends the current match.
- `/increaseHomeScore` : increases the home points by 1 point.
- `/increaseOutScore` : increases the out points by 1 point.
