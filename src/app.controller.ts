import {Controller, Get} from '@nestjs/common';
import * as WebSocket from 'ws';
import randomAlphabetic from './randomAlphabetic';

@Controller()
export class AppController {
    private homeScore: number = 0;
    private outScore: number = 0;

    @Get('/startMatch')
    startMatch(): string {
        const id = randomAlphabetic(20);
        this.homeScore = 0;
        this.outScore = 0;
        this.connectAndSendMessage({
            type: 'MATCH_STARTED',
            payload: id
        })
        return 'Started match with id: ' + id;
    }

    @Get('/endMatch')
    endMatch(): string {
        this.connectAndSendMessage({
            type: 'MATCH_ENDED'
        })
        return 'Ended match!';
    }

    @Get('/increaseHomeScore')
    increaseHomeScore(): number {
        this.homeScore++;
        this.connectAndSendMessage({
            type: 'GOAL_SCORED',
            payload: {
                homePoints: this.homeScore,
                outPoints: this.outScore
            }
        });
        return this.homeScore;
    }

    @Get('/increaseOutScore')
    increaseOutScore(): number {
        this.outScore++;
        this.connectAndSendMessage({
            type: 'GOAL_SCORED',
            payload: {
                homePoints: this.homeScore,
                outPoints: this.outScore
            }
        });
        return this.outScore;
    }

    private connectAndSendMessage(message: any) {
        const socket = new WebSocket('ws://localhost:3000');
        socket.onopen = () => {
            socket.send(JSON.stringify({
                event: 'msgToServer',
                data: message
            }));
            socket.close();
        }

    }
}
